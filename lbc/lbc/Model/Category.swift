//
//  Category.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

struct Category {
    let id: Int
    let name: String
}
