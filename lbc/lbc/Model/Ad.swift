//
//  Ad.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

import UIKit

struct Ad {
    let id: Int
    let category: Category
    let title: String
    let description: String
    let price: Double
    let smallImageUrl: URL?
    let thumbImageUrl: URL?
    let creationDate: Date
    let isUrgent: Bool
    let siret: String?
}
