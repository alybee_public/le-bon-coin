//
//  AdViewModel.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

protocol AdViewModelProtocol {
    func getAds(completion: @escaping (_ error: NSError?) -> Void)
}

class AdViewModel: AdViewModelProtocol {
    private let adApi: ClassifiedAdAPIProtocol
    private let categoryApi: CategoryAPIProtocol
    var ads:[Ad]
    private var categories: [Int: Category]

    init(adApi: ClassifiedAdAPIProtocol, categoryApi: CategoryAPIProtocol) {
        self.adApi = adApi
        self.categoryApi = categoryApi
        self.ads = [Ad]()
        self.categories = [Int: Category]()
    }

    // MARK: - Ads
    
    func getAds(completion: @escaping (_ error: NSError?) -> Void) {
        self.adApi.getList() { result in
            switch result {
            case .success(let list):
                self.getCategories { error in
                    if let error {
                        completion(error)
                        return
                    }

                    for item in list {
                        self.getCachedCategory(id: item.categoryId) { result in
                            switch result {
                            case .success(let category):
                                var smallImage:URL?, thumbImage:URL?
                                if let smallImageUrl = item.imagesURL.small, let thumbImageUrl = item.imagesURL.thumb {
                                    smallImage = URL(string: smallImageUrl)
                                    thumbImage = URL(string: thumbImageUrl)
                                }

                                let ad = Ad(id: item.id,
                                            category: category,
                                            title: item.title,
                                            description: item.description,
                                            price: Double(item.price),
                                            smallImageUrl:smallImage,
                                            thumbImageUrl:thumbImage,
                                            creationDate: item.creationDate,
                                            isUrgent: item.isUrgent,
                                            siret: item.siret)
                                self.ads.append(ad)
                            case .failure(_):
                                completion(CustomError.fetchError as NSError)
                                return
                            }
                        }
                    }
                    completion(nil)
                }
            case .failure(_):
                completion(CustomError.fetchError as NSError)
            }
        }
    }

    // MARK: - Category

    func getCachedCategory(id: Int, completion: @escaping (Result<Category, NSError>) -> Void) {
        guard let category = self.categories[id] else {
            completion(.failure(CustomError.default as NSError))
            return
        }

        completion(.success(category))
    }

    func getCategories(completion: @escaping (_ error: NSError?) -> Void) {
        if self.categories.isEmpty {
            self.categoryApi.getList() { result in
                switch result {
                case .success(let list):
                    for item in list {
                        self.categories[item.id] = Category(
                            id: item.id,
                            name: item.name
                        )
                    }
                    completion(nil)
                case .failure(_):
                    completion(CustomError.fetchError as NSError)
                }
            }
        }
    }
}
