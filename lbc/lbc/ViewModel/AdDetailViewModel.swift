//
//  AdDetailViewModel.swift
//  lbc
//
//  Created by Alison on 10/05/2023.
//

import Foundation

protocol AdDetailViewModelProtocol {

}

class AdDetailViewModel: AdDetailViewModelProtocol {
    var ad:Ad

    init(ad: Ad) {
        self.ad = ad
    }
}
