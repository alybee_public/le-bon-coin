//
//  UIFont+Ext.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

import UIKit

extension UIFont {
    /**
     Get Regular Font

     - Parameters:
        - style: The desired style
     - Returns: A font with the desire size for the style
     */
    class func regularFont(forTextStyle style: UIFont.TextStyle) -> UIFont {
        let customFont: UIFont
        let fontName = Config.sharedInstance.mainRegularFontName
        switch style {
        case .headline: customFont = UIFont(name: fontName, size: 15)!
        case .body: customFont = UIFont(name: fontName, size: 13)!
        default: customFont = UIFont(name: fontName, size: 11)!
        }
        let metrics = UIFontMetrics(forTextStyle: style)
        let scaledFont = metrics.scaledFont(for: customFont)

        return scaledFont
    }
    /**
     Get Bold Font

     - Parameters:
        - style: The desired style
     - Returns: A font with the desire size for the style
     */
    class func boldFont(forTextStyle style: UIFont.TextStyle) -> UIFont {
        let customFont: UIFont
        let fontName = Config.sharedInstance.mainBoldFontName
        switch style {
        case .title2: customFont = UIFont(name: fontName, size: 18)!
        case .body: customFont = UIFont(name: fontName, size: 13)!
        default: customFont = UIFont(name: fontName, size: 11)!
        }
        let metrics = UIFontMetrics(forTextStyle: style)
        let scaledFont = metrics.scaledFont(for: customFont)

        return scaledFont
    }
}
