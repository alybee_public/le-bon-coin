//
//  CustomError.swift
//  lwc-ios
//
//  Created by Alison on 31/01/2023.
//

import Foundation

enum CustomError: Error {
    case `default`
    case noConnexion
    case fetchError
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .fetchError:
            return "error_fetch_description".localized
        case .noConnexion:
            return "error_no_connexion".localized
        default:
            return "error_default_description".localized
        }
    }

    public var failureReason: String? {
        return "error_default_failure_reason".localized
    }

    public var recoverySuggestion: String? {
        return "error_default_recovery_suggestion".localized
    }
}

extension CustomError: CustomNSError {
    public static var errorDomain: String {
        return "fr.alybee.lbc"
    }

    public var errorCode: Int {
        switch self {
        case .default:
            return 0
        case .noConnexion:
            return 1
        case .fetchError:
            return 2
        }
    }
}
