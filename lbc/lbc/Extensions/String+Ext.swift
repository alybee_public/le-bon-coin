//
//  String+Ext.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
