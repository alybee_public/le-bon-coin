//
//  CustomDateFormatter.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

class CustomDateFormatter {

    enum DateFormatterStyle  {
        case fullDate   /// print d MMM y
        case shortDate  /// print dd/mm/yyyy
    }

    static func formatDate(date: Date, style: DateFormatterStyle) -> String {
        let formatter = DateFormatter()

        switch style {
        case .fullDate:
            formatter.dateFormat = "d MMM y"
        case .shortDate:
            formatter.dateStyle = .short
        }
        return formatter.string(from: date)
    }

    static func stringToDate(dateString: String) -> Date? {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.date(from: dateString)
    }
}
