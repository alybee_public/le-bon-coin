//
//  Config.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

class Config {

    let settings: NSDictionary

    static let sharedInstance = Config()

    private init() {
        let url = Bundle.main.url(forResource: "Config", withExtension: "plist")!
        settings = NSDictionary(contentsOf: url)!
    }

    // MARK: - Font

    var mainRegularFontName: String {
        let fonts: [String: Any]? = settings["font"] as? Dictionary
        return fonts?["main.regular"] as? String ?? ""
    }

    var mainBoldFontName: String {
        let fonts: [String: Any]? = settings["font"] as? Dictionary
        return fonts?["main.bold"] as? String ?? ""
    }

    // MARK: - API

    var apiUrl: String? {
        let apiSettings: [String: Any]? = settings["api"] as? Dictionary
        let url: [String: Any]? = apiSettings?["url"] as? Dictionary
        return url?["dev"] as? String ?? nil
    }
}
