//
//  AppAppearance.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

import Foundation
import UIKit

final class AppAppearance {
    static func initAppearance() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithTransparentBackground()
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().tintColor = UIColor(named: "NavigationColor")
    }
}
