//
//  AppDelegate.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppAppearance.initAppearance()

        window = UIWindow(frame: UIScreen.main.bounds)
        let homeVC = AdListViewController()
        let navigationController = UINavigationController(rootViewController: homeVC)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

extension AppDelegate {
    static var shared: AppDelegate {
        return (UIApplication.shared.delegate as? AppDelegate)!
    }

    var photoCacheManager: PhotosCacheManager {
        return PhotosCacheManager()
    }
}
