//
//  RestAPI.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

/// http method enum
enum HttpMethod: String {
    case GET
    case PUT
    case POST
    case DELETE
}

protocol URLSessionDataTaskProtocol {
    func resume()
}

protocol URLSessionProtocol {
    func dataTask(
        with: URLRequest,
        completion: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> any URLSessionDataTaskProtocol
}

extension URLSessionDataTask: URLSessionDataTaskProtocol {}

extension URLSession: URLSessionProtocol {
    func dataTask(with: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol {
        return dataTask(with: with, completionHandler: completion)
    }
}

protocol RestAPIProtocol {
    func request(path: String,
                 httpMethod: HttpMethod,
                 variables: NSObject?,
                 completion: @escaping (Result<Any, NSError>) -> Void)
}

class RestAPI: RestAPIProtocol {
    /// URLSession
    private let session: any URLSessionProtocol

    /// base url for all API Call
    private let baseURL: String

    /**
    Initialize the Rest API

    - Parameters:
        - session: URLSession
        - baseURL: base url
    */
    init(session: any URLSessionProtocol, baseURL: String) {
        self.session = session
        self.baseURL = baseURL
    }

    /**
    Create the rest api request

     - Parameters:
        - path: Path of the desired API,
        - httpMethod: Http method for API Call
        - variables: variables which need to be include on the API Call
        - completion: block containing the json response or the error
     */
    func request(path: String,
                 httpMethod: HttpMethod,
                 variables: NSObject? = nil,
                 completion: @escaping (Result<Any, NSError>) -> Void) {
        guard let url = URL(string: self.baseURL + "/" + path) else {
            let customError = CustomError.default
            completion(.failure(customError as NSError))
            return
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue

        let task = self.session.dataTask(with: urlRequest) {
            (data, response, error) in
            let customError = CustomError.default

            guard let data, error == nil else {
                completion(.failure(customError as NSError))
                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                completion(.success(json))
            } catch {
                completion(.failure(customError as NSError))
            }
        }

        task.resume()
    }
}
