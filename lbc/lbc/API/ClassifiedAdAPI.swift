//
//  AdAPI.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

protocol ClassifiedAdAPIProtocol {
    func getList(completion: @escaping (Result<[ClassifiedAdDTO], NSError>) -> Void)
}

class ClassifiedAdAPI: ClassifiedAdAPIProtocol {
    private let client: any RestAPIProtocol
    
    init(client: any RestAPIProtocol) {
        self.client = client
    }
    
    func getList(completion: @escaping (Result<[ClassifiedAdDTO], NSError>) -> Void) {
        self.client.request(
            path: "listing.json",
            httpMethod: .GET,
            variables: nil
        ) {
            result in
            switch(result) {
            case .success(let data):
                guard let json = data as? [[String: Any]] else {
                    completion(.failure(CustomError.fetchError as NSError))
                    return
                }
            
                var ret = [ClassifiedAdDTO]()
                for classifiedAd in json {
                    if
                        let id = classifiedAd["id"] as? Int,
                        let title = classifiedAd["title"] as? String,
                        let categoryId = classifiedAd["category_id"] as? Int,
                        let creationDateStr = classifiedAd["creation_date"] as? String,
                        let creationDate = CustomDateFormatter.stringToDate(dateString: creationDateStr),
                        let description = classifiedAd["description"] as? String,
                        let isUrgent = classifiedAd["is_urgent"] as? Bool,
                        let imagesURL = classifiedAd["images_url"] as? [String: Any],
                        let price = classifiedAd["price"] as? Float {
                        
                        let imagesURLDTO = ImagesURLDTO(
                            small: imagesURL["small"] as? String,
                            thumb: imagesURL["thumb"] as? String
                        )
                        
                        let classifiedAdDTO = ClassifiedAdDTO(
                            id: id,
                            title: title,
                            categoryId: categoryId,
                            creationDate: creationDate,
                            description: description,
                            isUrgent: isUrgent,
                            imagesURL: imagesURLDTO,
                            price: price,
                            siret: classifiedAd["siret"] as? String
                        )
                        
                        ret.append(classifiedAdDTO)
                    } else {
                        completion(.failure(CustomError.fetchError as NSError))
                        return
                    }
                }
            
                completion(.success(ret))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
