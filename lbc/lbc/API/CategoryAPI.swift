//
//  CategoryAPI.swift
//  lbc
//
//  Created by alison on 09/05/2023.
//

import Foundation

protocol CategoryAPIProtocol {
    func getList(completion: @escaping (Result<[CategoryDTO], NSError>) -> Void)
}

class CategoryAPI: CategoryAPIProtocol {
    private let client: any RestAPIProtocol
    
    init(client: any RestAPIProtocol) {
        self.client = client
    }
    
    func getList(completion: @escaping (Result<[CategoryDTO], NSError>) -> Void) {
        self.client.request(
            path: "categories.json",
            httpMethod: .GET,
            variables: nil
        ) {
            result in
            switch(result) {
            case .success(let data):
                guard let json = data as? [[String: Any]] else {
                    completion(.failure(CustomError.default as NSError))
                    return
                }
            
                var ret = [CategoryDTO]()
                for category in json {
                    if
                        let id = category["id"] as? Int,
                        let name = category["name"] as? String {
                        
                        let categoryDTO = CategoryDTO(
                            id: id,
                            name: name
                        )
                        
                        ret.append(categoryDTO)
                    }
                    else {
                        completion(.failure(CustomError.fetchError as NSError))
                        return
                    }
                }
            
                completion(.success(ret))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
