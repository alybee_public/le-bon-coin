//
//  ImageURLDTO.swift
//  lbc
//
//  Created by alison on 09/05/2023.
//

import Foundation

struct ImagesURLDTO {
    let small: String?
    let thumb: String?
}
