//
//  AdDTO.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

struct ClassifiedAdDTO {
    let id: Int
    let title: String
    let categoryId: Int
    let creationDate: Date
    let description: String
    let isUrgent: Bool
    let imagesURL: ImagesURLDTO
    let price: Float
    let siret: String?
}
