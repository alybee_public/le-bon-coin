//
//  CategoryDTO.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import Foundation

struct CategoryDTO {
    let id: Int
    let name: String
}
