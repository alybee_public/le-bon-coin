//
//  AdListViewController.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

import UIKit

class AdListViewController: UIViewController {

    private var viewModel: AdViewModel!
    
    // MARK: - UI Items

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = UIColor(named: "BackgroundColor")
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData()
        initUI()
        refreshAds()
    }

    // MARK: - Data

    func initData() {
        let client = RestAPI(session: URLSession.shared as URLSessionProtocol, baseURL: Config.sharedInstance.apiUrl!)
        let adApi = ClassifiedAdAPI(client: client)
        let categoryApi = CategoryAPI(client: client)
        self.viewModel = AdViewModel(adApi: adApi, categoryApi: categoryApi)
    }

    func refreshAds() {
        self.view.showLoading()
        
        viewModel.getAds(completion: { error in
            DispatchQueue.main.async {
                self.view.stopLoading()
                if let error {
                    let alert = UIAlertController(title: error.localizedDescription,
                                                  message: error.localizedRecoverySuggestion,
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "retry".localized,style: .default, handler: {(_: UIAlertAction!) in
                        self.refreshAds()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                self.tableView.reloadData()
            }
        })
    }

    // MARK: - UI

    func initUI() {
        // init background
        self.view.layerGradient(startPoint: .topLeft,
                                endPoint: .bottomRight,
                                colorArray: [(UIColor(named: "HomeBackgroundColorFirst") ?? .white).cgColor,
                                             (UIColor(named: "HomeBackgroundColorLast") ?? .white).cgColor],
                                type: .axial)

        // init header
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        let logoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        logoView.contentMode = .scaleAspectFit
        logoView.tintColor = .white
        logoView.image = UIImage(named: "Leboncoin_Logo")
        logoContainer.addSubview(logoView)
        self.navigationItem.titleView = logoContainer

        // init tableView
        setTableView()
    }

    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(AdTableViewCell.self, forCellReuseIdentifier: "AdTableViewCell")
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
}

extension AdListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.ads.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AdTableViewCell.self),
                                                 for: indexPath) as! AdTableViewCell
        let ad = viewModel.ads[indexPath.row]
        cell.categoryLabel.text = ad.category.name
        cell.titleLabel.text = ad.title
        cell.titleLabel.sizeToFit()
        cell.priceLabel.text = String(ad.price) + "price_unit".localized
        cell.isUrgentLabel.isHidden = !ad.isUrgent

        if let imageUrl = ad.smallImageUrl {
            AppDelegate.shared.photoCacheManager.getImage(url: imageUrl) { result in
                switch result {
                case .success(let image):
                    cell.adImageView.image = image
                default:
                    break
                }
            }
        }

        return cell
    }
}

extension AdListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let adDetailView = AdDetailViewController()
        let ad = viewModel.ads[indexPath.row]
        self.navigationController?.pushViewController(adDetailView, animated: true)
        adDetailView.loadAdDetail(ad: ad)
    }
}
