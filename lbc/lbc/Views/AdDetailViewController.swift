//
//  AdDetailViewController.swift
//  lbc
//
//  Created by Alison on 08/05/2023.
//

import UIKit

class AdDetailViewController: UIViewController {

    private var viewModel: AdDetailViewModel!

    // MARK: - UI Items

    let adImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "small_image_placeholder")
        return imageView
    }()

    let adTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(named: "TextColor")
        label.font = UIFont.boldFont(forTextStyle: .title2)
        label.numberOfLines = 0
        return label
    }()

    let adDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(named: "TextColor")
        label.font = UIFont.regularFont(forTextStyle: .body)
        return label
    }()

    let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "AccentColor")
        label.font = UIFont.boldFont(forTextStyle: .title2)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let categoryLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "TextColor")
        label.font = UIFont.regularFont(forTextStyle: .body)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "TextColor")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.regularFont(forTextStyle: .body)
        return label
    }()

    let siretLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "TextColor")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.regularFont(forTextStyle: .body)
        return label
    }()

    let isUrgentLabel: UILabel = {
        let label = UILabel()
        label.text = "urgent".localized
        label.font = UIFont.boldFont(forTextStyle: .body)
        label.textColor =  .white
        label.backgroundColor =  UIColor(named: "AccentColor")
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        let paddedWidth = label.intrinsicContentSize.width + 2 * 10
        label.widthAnchor.constraint(equalToConstant: paddedWidth).isActive = true
        label.textAlignment = .center
        return label
    }()

    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()

    let contentView = UIView()

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }

    // MARK: Ad

    func loadAdDetail(ad: Ad) {
        viewModel = AdDetailViewModel(ad: ad)
        displayAd()
    }

    func displayAd() {
        adTitleLabel.text = viewModel.ad.title
        priceLabel.text = String(viewModel.ad.price) + "price_unit".localized
        isUrgentLabel.isHidden = !viewModel.ad.isUrgent
        descriptionLabel.text = viewModel.ad.description
        adDateLabel.text = CustomDateFormatter.formatDate(date: viewModel.ad.creationDate, style: .fullDate)
        categoryLabel.text = viewModel.ad.category.name
        if let siret = viewModel.ad.siret {
            siretLabel.text = "siret".localized.uppercased() + " " + siret
            siretLabel.isHidden = false
        } else {
            siretLabel.isHidden = true
        }

        if let imageUrl = viewModel.ad.thumbImageUrl {
            AppDelegate.shared.photoCacheManager.getImage(url: imageUrl) { result in
                switch result {
                case .success(let image):
                    self.adImageView.image = image
                default:
                    break
                }
            }
        }
    }

    // MARK: UI

    func initUI() {
        self.view.backgroundColor = UIColor(named: "BackgroundColor")

        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(adImageView)
        contentView.addSubview(adTitleLabel)
        contentView.addSubview(adDateLabel)
        contentView.addSubview(categoryLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(isUrgentLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(siretLabel)

        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),

            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),

            adImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            adImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            adImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.8),
            adImageView.heightAnchor.constraint(equalToConstant: 200),

            adTitleLabel.topAnchor.constraint(equalTo: adImageView.bottomAnchor, constant: 20),
            adTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            adTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            priceLabel.topAnchor.constraint(equalTo: adTitleLabel.bottomAnchor, constant: 20),
            priceLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),

            isUrgentLabel.topAnchor.constraint(equalTo: adTitleLabel.bottomAnchor, constant: 20 + 2),
            isUrgentLabel.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 10),
            isUrgentLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -20),

            adDateLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 20),
            adDateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),

            categoryLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 20),
            categoryLabel.leadingAnchor.constraint(equalTo: adDateLabel.trailingAnchor, constant: 10),
            categoryLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -20),

            descriptionLabel.topAnchor.constraint(equalTo: adDateLabel.bottomAnchor, constant: 20),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),

            siretLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 20),
            siretLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            siretLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            siretLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ])
    }
}
