//
//  AdTableViewCell.swift
//  lbc
//
//  Created by Alison on 09/05/2023.
//

import UIKit

class AdTableViewCell: UITableViewCell {

    var adImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "thumb_image_placeholder")
        return image
    }()

    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "TextColor")
        label.font = UIFont.boldFont(forTextStyle: .title2)
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "AccentColor")
        label.font = UIFont.boldFont(forTextStyle: .title2)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var categoryLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "TextColor")
        label.font = UIFont.regularFont(forTextStyle: .headline)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var isUrgentLabel: UILabel = {
        let label = UILabel()
        label.text = "urgent".localized
        label.font = UIFont.boldFont(forTextStyle: .body)
        label.textColor =  .white
        label.backgroundColor =  UIColor(named: "AccentColor")
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        let paddedWidth = label.intrinsicContentSize.width + 2 * 10
        label.widthAnchor.constraint(equalToConstant: paddedWidth).isActive = true
        label.textAlignment = .center
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let bgColorView = UIView()
        bgColorView.backgroundColor =  UIColor(named: "NavigationColor")?.withAlphaComponent(0.2)
        self.selectedBackgroundView = bgColorView

        contentView.addSubview(adImageView)
        NSLayoutConstraint.activate([
            adImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            adImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            adImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            adImageView.widthAnchor.constraint(equalToConstant: 80),
            adImageView.heightAnchor.constraint(equalToConstant: 100)
        ])

        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: adImageView.trailingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
        ])

        contentView.addSubview(categoryLabel)
        NSLayoutConstraint.activate([
            categoryLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            categoryLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            categoryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)
        ])

        contentView.addSubview(priceLabel)
        NSLayoutConstraint.activate([
            priceLabel.leadingAnchor.constraint(equalTo: categoryLabel.leadingAnchor),
            priceLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 10),
            priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
        ])

        contentView.addSubview(isUrgentLabel)
        NSLayoutConstraint.activate([
            isUrgentLabel.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 10),
            isUrgentLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: 10),
            isUrgentLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            isUrgentLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -20)
        ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
