//
//  PhotosCacheManager.swift
//  lbc
//
//  Created by Alison on 10/05/2023.
//

import Foundation
import UIKit

protocol PhotosCacheProtocol {
    func getImage(url: URL, completion: @escaping (Result<UIImage, NSError>) -> Void)
}

class PhotosCacheManager: PhotosCacheProtocol {
    private let cache: NSCache<NSString, UIImage>
    
    init() {
        self.cache = NSCache<NSString, UIImage>()
    }
    
    func getImage(url: URL, completion: @escaping (Result<UIImage, NSError>) -> Void) {
        let key = url.absoluteString as NSString
        if let image = self.cache.object(forKey: key) {
            completion(.success(image))
            return
        }
        
        DispatchQueue.global(qos: .utility).async {
            guard
                let data = try? Data(contentsOf: url),
                let image = UIImage(data: data)
            else {
                completion(.failure(CustomError.default as NSError))
                return
            }
            
            DispatchQueue.main.async {
                self.cache.setObject(image, forKey: key)
                completion(.success(image))
            }
        }
    }
}
