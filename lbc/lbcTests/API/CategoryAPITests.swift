//
//  CategoryAPITests.swift
//  lbcTests
//
//  Created by Alison on 09/05/2023.
//

import XCTest
@testable import lbc

final class CategoryAPITests: XCTestCase {
    
    func testGetListOk() throws {
        let client = RestAPIMock(data: [
            ["id": 1, "name": "cat1"] as [String : Any],
            ["id": 2, "name": "cat2"] as [String : Any]
        ])
        
        let api = CategoryAPI(client: client)
        let exp = expectation(description: "wait for fetch completion")
        api.getList() {
            result in
            switch result {
            case .success(let categories):
                XCTAssertEqual(categories.count, 2)
                XCTAssertEqual(categories[0].id, 1)
                XCTAssertEqual(categories[0].name, "cat1")
                XCTAssertEqual(categories[1].id, 2)
                XCTAssertEqual(categories[1].name, "cat2")
            case .failure(_):
                XCTFail("request error")
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3)
    }
    
}
