//
//  ClassifiedAdAPITests.swift
//  lbcTests
//
//  Created by Alison on 09/05/2023.
//

import XCTest
@testable import lbc

final class ClassifiedAdAPITests: XCTestCase {
    
    func testGetListOk() throws {
        let client = RestAPIMock(data: [
            [
                "id": 1,
                "title": "cat1",
                "category_id": 1,
                "creation_date": "2023-01-01T00:00:00Z",
                "description": "desc1",
                "is_urgent": false,
                "images_url": [
                    "small": "https://leboncoin.fr/images/small1.png",
                    "thumb": "https://leboncoin.fr/images/thumb1.png"
                ],
                "price": 123.45 as Float
            ] as [String : Any],
            [
                "id": 2,
                "title": "cat2",
                "category_id": 2,
                "creation_date": "2023-01-01T00:00:00Z",
                "description": "desc2",
                "is_urgent": true,
                "images_url": [
                    "small": "https://leboncoin.fr/images/small2.png",
                    "thumb": "https://leboncoin.fr/images/thumb2.png"
                ],
                "price": 456.78 as Float,
                "siret": "123456789"
            ] as [String : Any]
        ])
        
        let api = ClassifiedAdAPI(client: client)
        let exp = expectation(description: "wait for fetch completion")
        api.getList {
            result in
            switch result {
            case .success(let ads):
                XCTAssertEqual(ads.count, 2)
                XCTAssertEqual(ads[0].id, 1)
                XCTAssertEqual(ads[0].title, "cat1")
                XCTAssertEqual(ads[0].creationDate, ISO8601DateFormatter().date(from: "2023-01-01T00:00:00Z"))
                XCTAssertEqual(ads[0].categoryId, 1)
                XCTAssertEqual(ads[0].isUrgent, false)
                XCTAssertEqual(ads[0].imagesURL.small, "https://leboncoin.fr/images/small1.png")
                XCTAssertEqual(ads[0].imagesURL.thumb, "https://leboncoin.fr/images/thumb1.png")
                XCTAssertEqual(ads[0].price, 123.45, accuracy: 0.001)
                XCTAssertNil(ads[0].siret)
                XCTAssertEqual(ads[1].id, 2)
                XCTAssertEqual(ads[1].title, "cat2")
                XCTAssertEqual(ads[1].creationDate, ISO8601DateFormatter().date(from: "2023-01-01T00:00:00Z"))
                XCTAssertEqual(ads[1].categoryId, 2)
                XCTAssertEqual(ads[1].isUrgent, true)
                XCTAssertEqual(ads[1].imagesURL.small, "https://leboncoin.fr/images/small2.png")
                XCTAssertEqual(ads[1].imagesURL.thumb, "https://leboncoin.fr/images/thumb2.png")
                XCTAssertEqual(ads[1].price, 456.78, accuracy: 0.001)
                XCTAssertEqual(ads[1].siret, "123456789")
            case .failure(_):
                XCTFail("request error")
            }

            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3)
    }
    
}
