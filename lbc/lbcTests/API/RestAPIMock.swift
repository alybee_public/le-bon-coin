//
//  RestAPIMock.swift
//  lbcTests
//
//  Created by Alison on 09/05/2023.
//

import Foundation
@testable import lbc

class RestAPIMock: RestAPIProtocol {
    let data: Any
    
    init(data: Any) {
        self.data = data
    }
    
    func request(path: String, httpMethod: lbc.HttpMethod, variables: NSObject?, completion: @escaping (Result<Any, NSError>) -> Void) {
        completion(.success(self.data))
    }
}
