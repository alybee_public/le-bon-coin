//
//  RestAPITests.swift
//  RestAPI tests
//
//  Created by Alison on 08/05/2023.
//

import XCTest
@testable import lbc

final class RestAPITests: XCTestCase {

    func testRequestOk() throws {
        let data = Data("{\"foo\":\"bar\"}".utf8)
        let session = URLSessionMock(data: data, error: nil)
        let api = RestAPI(session: session, baseURL: "https://leboncoin.fr")
        let exp = expectation(description: "wait for request completion")
        api.request(path: "foo/bar", httpMethod: .GET) { result in
            switch result {
            case .success(let data):
                let json = data as! [String: Any]
                XCTAssertEqual(json["foo"] as! String, "bar")
            case .failure(_):
                XCTFail("request fail")
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3)
    }
    
    func testRequestError() throws {
        let session = URLSessionMock(data: nil, error: NSError())
        let api = RestAPI(session: session, baseURL: "https://leboncoin.fr")
        let exp = expectation(description: "wait for request completion")
        api.request(path: "mochi/wawa", httpMethod: .POST) {
            result in
            switch result {
            case .success(_):
                XCTFail("request should fail")
            case .failure(let error):
                XCTAssertNotNil(error as? CustomError)
                XCTAssertEqual(error.code, CustomError.default.errorCode)
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3)
    }
    
    func testRequestParseError() throws {
        let data = Data("{\"foo\"bar\"}".utf8)
        let session = URLSessionMock(data: data, error: nil)
        let api = RestAPI(session: session, baseURL: "https://leboncoin.fr")
        let exp = expectation(description: "wait for request completion")
        api.request(path: "flamingo", httpMethod: .GET) { result in
            switch result {
            case .success(_):
                XCTFail("request should fail")
            case .failure(let error):
                XCTAssertNotNil(error as? CustomError)
                XCTAssertEqual(error.code, CustomError.default.errorCode)
            }
            
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 3)
    }

}
