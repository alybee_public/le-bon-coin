//
//  URLSessionMock.swift
//  lbcTests
//
//  Created by Alison on 09/05/2023.
//

import Foundation
@testable import lbc

class URLSessionDataTaskMock: URLSessionDataTaskProtocol {
    private let completion: () -> Void

    init(completion: @escaping () -> Void) {
        self.completion = completion
    }

    func resume() {
        completion()
    }
}

class URLSessionMock: URLSessionProtocol {
    let data: Data?
    let error: Error?
    
    init(data: Data?, error: Error?) {
        self.data = data
        self.error = error
    }
    
    func dataTask(
        with: URLRequest,
        completion: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> any URLSessionDataTaskProtocol {
        return URLSessionDataTaskMock() {
            [weak self] in
            completion(self?.data, nil, self?.error)
        }
    }
}
