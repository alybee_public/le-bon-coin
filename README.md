# LeBonCoin demo app



## The project

This project is a iOS demo app for Le Bon Coin. It displays a list of ads and a detail screen for each ad.


## Main instructions

* Modern architecture
* Compliance with SOLID first principle of single responsibility
* Graphical interface with UIKit and autolayout without Storyboard
* Swift language
* No extern library
* Minimum build settings to iOS 14.0
* Fetch data with an API call
* Unit tests

## Details

* MVVM architecture with Protocol Oriented Programming
* Data models and separate DTOs
* Unit tests
* Dark Mode
* Mock up: https://xd.adobe.com/view/db58fbae-562f-4065-b279-86cf3848d353-8e0b/

## Improvement

Many improvements can be done to this POC, such as:

- [ ] [UI] improve images display consistency, specificaly in list screen
- [ ] [UX] add filter in the home screen, share action in the detail screen, and so on
- [ ] [UX] improve iPad experience
- [ ] [UX] improve accessibility
- [ ] add pagination for load ads function to improve perf
- [ ] switch to SwiftUI because coding UIKit in code is so loooong
- [ ] unit test for UI
- [ ] improve error management
- [ ] add comments and doc
